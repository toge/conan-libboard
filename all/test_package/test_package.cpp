#include <iostream>
#include <vector>
#include "Board.h"

int main() {
    auto board = LibBoard::Board {};
    board.setLineWidth(0.1);

    auto points = std::vector<LibBoard::Point> {};
    points.push_back(LibBoard::Point{100, 100});
    points.push_back(LibBoard::Point{200, 100});
    points.push_back(LibBoard::Point{100, 200});
    points.push_back(LibBoard::Point{200, 200});

    board.drawPolyline(points);

    return 0;
}
